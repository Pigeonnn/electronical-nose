import pika
import sys
import random
import time
from nos import dokonaj_pomiar

credentials = pika.PlainCredentials(username='admin', password='admin')
connection = pika.BlockingConnection(pika.ConnectionParameters(host='127.0.0.1',port=5672,credentials=credentials))

channel = connection.channel()
channel.queue_declare(queue='task_queue',durable=True)

while True:
    pomiar = dokonaj_pomiar()
    channel.basic_publish(exchange='',routing_key='task_queue',body=pomiar,properties=pika.BasicProperties(delivery_mode=2,))
    print ("Wyslano pomiar:")
    print(pomiar)
    print()
    #przerwa = random.randint(1,4)
    #time.sleep(przerwa)
connection.close()