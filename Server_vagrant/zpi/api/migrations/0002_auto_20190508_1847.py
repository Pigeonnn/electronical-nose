# Generated by Django 2.2 on 2019-05-08 18:47

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='measurements',
            name='category',
            field=models.CharField(choices=[('excellent', 'Excellent'), ('good', 'Good'), ('acceptable', 'Acceptable'), ('spoiled', 'Spoiled'), ('none', 'None')], default='none', max_length=10),
        ),
    ]
