from django.db import models
from django.core.cache import cache
from joblib import load
import pandas as pd
import os
import numpy as np
import datetime

# Create your models here.
class Measurements(models.Model):
    time = models.DateTimeField(auto_now_add=True)
    category = models.CharField(max_length=10, choices=(
        ('excellent','Excellent'),
        ('good','Good'),
        ('acceptable','Acceptable'),
        ('spoiled','Spoiled'),
        ('none','None')),
        default='none'
    )
    mq135 = models.FloatField()
    mq136 = models.FloatField()
    mq2 = models.FloatField()
    mq3 = models.FloatField()
    mq4 = models.FloatField()
    mq5 = models.FloatField()
    mq6 = models.FloatField()
    mq7 = models.FloatField()
    mq8 = models.FloatField()
    mq9 = models.FloatField()
    humidity = models.FloatField()
    temperature = models.FloatField()
      
    def save(self, *args, **kwargs):
        clf_cache_key = 'model_cache' 
        # this key is used to `set` and `get` 
        # your trained model from the cache

        clf = cache.get(clf_cache_key) # get model from cache

        if clf is None:
            # your model isn't in the cache
            # so `set` it
            clf_file = os.path.join('/vagrant/classifier', 'clf.joblib')
            clf = load(clf_file) # load model
            cache.set(clf_cache_key, clf, None) # save in the cache
            # in above line, None is the timeout parameter. It means cache forever

        # now predict
        data = np.array([self.mq135, self.mq136, self.mq2, self.mq3, self.mq4, self.mq5, self.mq6, self.mq7,
                self.mq8, self.mq9, self.humidity, self.temperature]).reshape(1,-1)

        self.category = clf.predict(data)[0]

        super().save(*args, **kwargs)
        #self.time = datetime.datetime.now()