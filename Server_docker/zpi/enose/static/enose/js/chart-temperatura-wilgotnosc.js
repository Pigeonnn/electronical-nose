var yLabels = ["Spoiled","Acceptable","Good","Excellent"];
var yLabelCounter=0;
var dps = []; // dataPoints
var chart = new CanvasJS.Chart("chartContainer", {
	
	axisY: {
		interval: 1,
		includeZero: false,
		minimum: 0,
		maximum: 3,
		labelFormatter: function ( e ) {
		var yCats = yLabels[yLabelCounter++];
		if(yLabelCounter === yLabels.length)
		yLabelCounter = 0;
		return yCats;
		}
	}, 
	toolTip:{
        enabled: false,       //disable here
        animationEnabled: true //disable here
    },
	data: [{
		type: "line",
		dataPoints: dps
	}]
});

var dataLength = 10; // number of dataPoints visible at any point

l = 1

var updateChart = function (data) {

	var category = capitalize(data.category);
	
	switch (category) {
		case 'Spoiled':
			category = 0;
			break;
		case 'Acceptable':
			category = 1;
			break;
		case 'Good':
			category = 2;
			break;
		case 'Excellent':
			category = 3;
			break;
	}
	
	var mydate = new Date(data.time);
	var godzina = ('0'+mydate.getHours()).slice(-2)+':'+('0'+mydate.getMinutes()).slice(-2);
	dps.push({
		label: godzina, y: category 
	});
	
	l=l+1;
	
	if (dps.length > dataLength) {
		dps.shift();
	}
	chart.render();
};