import pandas as pd
from sklearn.tree import DecisionTreeClassifier
from joblib import dump, load

data_train = pd.read_csv('train_test_data.csv')

clf = DecisionTreeClassifier(max_depth=6, random_state=1111)

clf.fit(data_train.drop(labels=['minute','class','TVC','Unnamed: 0'],axis=1), data_train['class'])

dump(clf, 'clf.joblib')