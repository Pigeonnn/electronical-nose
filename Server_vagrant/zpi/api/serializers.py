from rest_framework import serializers
from .models import Measurements

class MeasurementsSerializer(serializers.ModelSerializer):

    class Meta:
        model = Measurements
        fields = ('pk','time', 'category', 'mq135', 'mq136', 'mq2', 'mq3', 'mq4', 'mq5', 'mq6', 'mq7', 'mq8', 'mq9', 'humidity', 'temperature')
        read_only_fields = ('time','category')