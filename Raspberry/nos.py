from numpy import random
import pandas as pd


def dokonaj_pomiar():
    data_val = pd.read_csv ('validation_data.csv')
    data_val = data_val.drop (labels=['minute', 'TVC', 'Unnamed: 0'], axis=1)

    spoiled = data_val.loc[(data_val['class'] == 'spoiled')]
    good = data_val.loc[(data_val['class'] == 'good')]
    excellent = data_val.loc[(data_val['class'] == 'excellent')]
    acceptable = data_val.loc[(data_val['class'] == 'acceptable')]

    probability = random.uniform (0, 100)

    if (0<= probability <= 40):
        data_df = good.iloc[random.randint (0, len(good)), :]
    elif 40< probability <= 55:
        data_df = spoiled.iloc[random.randint (0, len(spoiled)), :]
    elif 55< probability <= 75:
        data_df = excellent.iloc[random.randint (0, len(excellent)), :]
    elif 75< probability <= 100:
        data_df = acceptable.iloc[random.randint (0, len(acceptable)), :]


    data_to_go = data_df.drop (labels=["class"])
    data_json = data_to_go.to_json (path_or_buf=None)
    return data_json
    