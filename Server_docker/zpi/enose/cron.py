def consume():
    import pika
    try:
        credentials = pika.PlainCredentials(username='admin', password='admin')
        connection = pika.BlockingConnection(pika.ConnectionParameters(host='127.0.0.1',port=5672,credentials=credentials))
        channel = connection.channel()
        channel.queue_declare(queue='task_queue', durable=True)
        channel.basic_qos(prefetch_count=1)
        channel.basic_consume(queue='task_queue', on_message_callback=callback)
        channel.start_consuming()
    except:
        pass


def callback(ch, method, properties, body):
    import random
    import time
    import requests
    import pandas as pd
    REST_API_URL = 'http://127.0.0.1:8000/api/measurements/'
    ch.basic_ack(delivery_tag=method.delivery_tag)
    js = pd.read_json(body, typ='series')
    js = pd.DataFrame([js])
    data_json = bytes(js.to_json(orient='records'), encoding='utf-8')
    headers = {'content-type':'application/json'}
    req = requests.post(REST_API_URL, data_json)
    pause = random.randint(1,5)
    time.sleep(pause)

'''
def consume():
    import pika
    import random
    import time
    import requests
    REST_API_URL = 'https://127.0.0.1:8000/api/measurements/'

    connection = pika.BlockingConnection(
    pika.ConnectionParameters(host='localhost'))
    channel = connection.channel()

    channel.queue_declare(queue='task_queue', durable=True)

    channel.basic_qos(prefetch_count=1)
    channel.basic_consume(queue='task_queue', on_message_callback=callback)

    channel.start_consuming()
'''
