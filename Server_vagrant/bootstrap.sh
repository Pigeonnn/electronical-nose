#!/usr/bin/env bash
apt-get update
cd /vagrant/zpi
apt install -y python3-pip
pip3 install -r ../requirements.txt
pip3 install django-crontab
sudo chmod -R 777 /etc/apt
echo "deb http://apt.postgresql.org/pub/repos/apt/ stretch-pgdg main" > /etc/apt/sources.list.d/pgdg.list
wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | sudo apt-key add -
sudo apt-get update
sudo apt-get -y install postgresql-10 postgresql-client-10
su postgres << EOF
psql -c "
  CREATE DATABASE zpi;
  "
EOF
su postgres << EOF
psql -c "
  CREATE USER zpiuser WITH PASSWORD 'password';
  "
EOF
su postgres << EOF
psql -c "
  ALTER ROLE zpiuser SET client_encoding TO 'utf8';
  "
EOF
su postgres << EOF
psql -c "
  ALTER ROLE zpiuser SET default_transaction_isolation TO 'read committed';
  "
EOF
su postgres << EOF
psql -c "
  ALTER ROLE zpiuser SET timezone TO 'UTC';
  "
EOF
su postgres << EOF
psql -c "
  GRANT ALL PRIVILEGES ON DATABASE zpi TO zpiuser;
  "
EOF
sudo apt-get -y install libpq-dev python-dev
pip3 install psycopg2
python3 manage.py makemigrations
python3 manage.py migrate
python3 manage.py crontab add
python3 manage.py runserver 0.0.0.0:8000

sudo apt-get -y install dirmngr --install-recommends

sudo apt-key adv --keyserver "hkps.pool.sks-keyservers.net" --recv-keys "0x6B73A36E6026DFCA"
sudo apt-get install apt-transport-https

sudo tee /etc/apt/sources.list.d/bintray.rabbitmq.list <<EOF
deb https://dl.bintray.com/rabbitmq-erlang/debian bionic erlang-21.x
deb https://dl.bintray.com/rabbitmq/debian bionic main
EOF

sudo apt-get update -y

sudo apt-get install rabbitmq-server -y --fix-missing

sudo rabbitmq-plugins enable rabbitmq_management

sudo systemctl enable rabbitmq-server
sudo systemctl start rabbitmq-server

sudo rabbitmqctl add_user admin admin 
sudo rabbitmqctl set_user_tags admin administrator
sudo rabbitmqctl set_permissions -p / admin ".*" ".*" ".*"