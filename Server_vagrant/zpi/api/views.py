from django.shortcuts import render
from rest_framework import generics, viewsets
from .serializers import MeasurementsSerializer
from .models import Measurements
from rest_framework.views import APIView
from rest_framework.response import Response
from django.http import Http404
from django.conf import settings
import random
import time
import threading
import pika

# Create your views here.

class CreateView(generics.ListCreateAPIView):
    queryset = Measurements.objects.all()
    serializer_class = MeasurementsSerializer

    def perform_create(self, serializer):
        serializer.save()

class DetailsView(generics.RetrieveUpdateDestroyAPIView):
    """This class handles the http GET, PUT and DELETE requests."""

    queryset = Measurements.objects.all()
    serializer_class = MeasurementsSerializer

class MeasurementsViewSet(viewsets.ViewSet):
    
    def retrieve(self, request, fpk=None, lpk=None):
        queryset = Measurements.objects.all()[fpk:lpk]
        serializer = MeasurementsSerializer(queryset, many=True)
        return Response(serializer.data)

class MeasurementsViewLast(viewsets.ViewSet):
    
    def retrieve(self, request):
        queryset = Measurements.objects.last()
        serializer = MeasurementsSerializer(queryset)
        return Response(serializer.data)