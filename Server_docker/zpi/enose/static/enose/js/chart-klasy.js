new Chart(document.getElementById("doughnut-chart"), {
    type: 'doughnut',
    data: {
      labels: ["Good", "Spoiled", "Excellent", "Acceptable"],
      datasets: [
        {
          backgroundColor: ["#3e95cd", "#8e5ea2","#3cba9f","#e8c3b9","#c45850"],
          data: [15,5,8,6]
        }
      ]
    },
    options: {
    }
});