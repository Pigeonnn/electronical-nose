from django.urls import path
from rest_framework.urlpatterns import format_suffix_patterns
from .views import CreateView, DetailsView, MeasurementsViewSet, MeasurementsViewLast

urlpatterns = {
    path('measurements/', CreateView.as_view(), name="create"),
    path('measurements/<int:pk>/',
        DetailsView.as_view(), name="details"),
    path('measurements/set/<int:fpk>-<int:lpk>/',
        MeasurementsViewSet.as_view({'get': 'retrieve'}), name="set"),
    path('measurements/last/',
        MeasurementsViewLast.as_view({'get': 'retrieve'}), name="last"),
}

urlpatterns = format_suffix_patterns(urlpatterns, allowed=['json', 'html'])