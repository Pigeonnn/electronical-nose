const capitalize = (s) => {
  if (typeof s !== 'string') return ''
  return s.charAt(0).toUpperCase() + s.slice(1)
}

pk = 1;

function deleteWarning(e) {
  e.parentNode.parentNode.removeChild(e.parentNode);
}

function addWarning(data) {
  var mydate = new Date(data.time);
  var newnode = document.createElement('div');
  newnode.innerHTML = `
    <div class="warning">
    <i class="fa fa-exclamation-triangle" aria-hidden="true"></i>
                    | `+('0'+mydate.getHours()).slice(-2)+':'+('0'+mydate.getMinutes()).slice(-2)+` | `+data.category+`
                    | `+data.temperature+'&deg;C'+` | `+data.humidity+'%'+` |
                    <button style="background:none;border:none;opacity:0.5;" onclick="deleteWarning(this)">x</button>
                </div>
  `;
  var parentnode = document.getElementById('panel-body-warning');
  parentnode.insertBefore(newnode, parentnode.children[0])
}

function updateData(data) {
  document.getElementById('klasa').innerHTML = capitalize(data.category);
  document.getElementById('temperatura').innerHTML = data.temperature+'&deg;C';
  document.getElementById('wilgotnosc').innerHTML = data.humidity+'%';
  var mydate = new Date(data.time);
  document.getElementById('godzinapomiaru').innerHTML = ('0'+mydate.getHours()).slice(-2)+':'+('0'+mydate.getMinutes()).slice(-2);
  document.getElementById('mq135').innerHTML = data.mq135;
  document.getElementById('mq136').innerHTML = data.mq136;
  document.getElementById('mq2').innerHTML = data.mq2;
  document.getElementById('mq3').innerHTML = data.mq3;
  document.getElementById('mq4').innerHTML = data.mq4;
  document.getElementById('mq5').innerHTML = data.mq5;
  document.getElementById('mq6').innerHTML = data.mq6;
  document.getElementById('mq7').innerHTML = data.mq7;
  document.getElementById('mq8').innerHTML = data.mq8;
  document.getElementById('mq9').innerHTML = data.mq9;
}

var getJSON = function(url, callback) {
    var xhr = new XMLHttpRequest();
    xhr.open('GET', url, true);
    xhr.responseType = 'json';
    xhr.onload = function() {
      var status = xhr.status;
      if (status === 200) {
        callback(null, xhr.response);
      } else {
        callback(status, xhr.response);
      }
    };
    xhr.send();
};

getJSON('http://127.0.0.1:8000/api/measurements/last/',
function(err, data) {
  if (err !== null) {
    //alert('Something went wrong: ' + err);
  } else {
    updateData(data);
    pk = data.pk;
    if (data.category == 'spoiled') { addWarning(data) };
	  updateChart(data)
  }
  
});

setInterval(function(){
  getJSON('http://127.0.0.1:8000/api/measurements/last/',
    function(err, data) {
      if (err !== null) {
        alert('Something went wrong: ' + err);
      } else if (data.pk > pk) {
        newpk = data.pk;
        while (pk < newpk) {
          pk=pk+1;
          getJSON('http://127.0.0.1:8000/api/measurements/'+pk+'/',
          function(err, data) {
            if (err !== null) {
              alert('Something went wrong: ' + err);
            } else {
              updateData(data);
              if (data.category == 'spoiled') { addWarning(data) };
			        updateChart(data)
            }
            })  
          }
        } else {}
    }) 
  }, 1000);