#!/usr/bin/env python
import pika
import time
import random
from datetime import datetime
import requests
import pandas as pd
import json
from sklearn.tree import DecisionTreeClassifier
from joblib import load

#REST_API_URL = 'https://api.powerbi.com/beta/d0da435b-a7e7-4d74-a4ae-f72cf8f3b2db/datasets/1ac53fea-971e-4acf-9c87-53e367c5e016/rows?key=Es6%2FV1h0OrItBhmTeYOXCLPSHEqYv%2FR8nT%2BWjNn3e1GKEA1%2F%2BL%2F2btYZ5t98QziQ32cOv2EsUyRPTLDboXiSwA%3D%3D'
#clf = load('clf.joblib')

def get_cl(classn):
    if classn == 'spoiled':
        cl = 0
    elif classn == 'acceptable':
        cl = 1
    elif classn == 'good':
        cl = 2
    else:
        cl = 3
    return cl
'''
def wysli_do_pbi(js):
    #wczytujemy json do dataframe
    js = pd.read_json(js, typ='series')
    js = pd.DataFrame([js])

    #dolaczamy kolumny
    js['class'] = clf.predict(js)
    js['cl'] = get_cl(js.iloc[0]['class'])
    js['Time'] = datetime.now().isoformat()

    #dataframe do json
    data_json = bytes(js.to_json(orient='records'), encoding='utf-8')
    print()
    print('Przygotowano do wyslania:')
    print(data_json)

    #wysylamy do powerbi
    req = requests.post(REST_API_URL, data_json)
    print("Dane wyslane do Power BI API")
'''

def callback(ch, method, properties, body):
    print('Odebrano pomiar:')
    print(body)
    ch.basic_ack(delivery_tag=method.delivery_tag)
    przerwa = random.randint(1,5)
    time.sleep(przerwa)

credentials = pika.PlainCredentials(username='admin', password='admin')
connection = pika.BlockingConnection(pika.ConnectionParameters(host='127.0.0.1',port=5672,credentials=credentials))
channel = connection.channel()
channel.queue_declare(queue='task_queue', durable=True)
print(' [*] Waiting for messages. To exit press CTRL+C')
channel.basic_qos(prefetch_count=1)
channel.basic_consume(queue='task_queue', on_message_callback=callback)
channel.start_consuming()